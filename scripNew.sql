USE [master]
GO
/****** Object:  Database [DailyServiceDB]    Script Date: 7/19/2016 3:09:45 PM ******/
CREATE DATABASE [DailyServiceDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DailyServiceDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSQL\MSSQL\DATA\DailyServiceDB.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DailyServiceDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSQL\MSSQL\DATA\DailyServiceDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DailyServiceDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DailyServiceDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DailyServiceDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DailyServiceDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DailyServiceDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DailyServiceDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DailyServiceDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [DailyServiceDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DailyServiceDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [DailyServiceDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DailyServiceDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DailyServiceDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DailyServiceDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DailyServiceDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DailyServiceDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DailyServiceDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DailyServiceDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DailyServiceDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DailyServiceDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DailyServiceDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DailyServiceDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DailyServiceDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DailyServiceDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DailyServiceDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DailyServiceDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DailyServiceDB] SET RECOVERY FULL 
GO
ALTER DATABASE [DailyServiceDB] SET  MULTI_USER 
GO
ALTER DATABASE [DailyServiceDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DailyServiceDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DailyServiceDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DailyServiceDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'DailyServiceDB', N'ON'
GO
USE [DailyServiceDB]
GO
/****** Object:  Table [dbo].[Area]    Script Date: 7/19/2016 3:09:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Area](
	[AreaId] [int] IDENTITY(1,1) NOT NULL,
	[AreaName] [varchar](100) NULL,
	[CityId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[City]    Script Date: 7/19/2016 3:09:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[City](
	[CityId] [int] IDENTITY(1,1) NOT NULL,
	[CityName] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[CityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Posts]    Script Date: 7/19/2016 3:09:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Posts](
	[PostId] [int] NOT NULL,
	[PostTitle] [nvarchar](255) NULL,
	[ProfessionId] [int] NOT NULL,
	[PostDescription] [nvarchar](max) NOT NULL,
	[ServiceSeekerId] [int] NOT NULL,
	[IsActive] [bit] NULL,
	[UpdateDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Profession]    Script Date: 7/19/2016 3:09:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profession](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProfName] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK__Professi__3214EC072CD63F4F] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Reviews]    Script Date: 7/19/2016 3:09:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reviews](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SproviderId] [int] NOT NULL,
	[ReviewCount] [int] NOT NULL,
	[GivenBy] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServiceProvider]    Script Date: 7/19/2016 3:09:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ServiceProvider](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[CityId] [int] NOT NULL,
	[AreaId] [int] NOT NULL,
	[ContactAddress] [varchar](500) NOT NULL,
	[ContactNo] [varchar](30) NOT NULL,
	[Image] [varchar](255) NULL,
	[Email] [varchar](255) NOT NULL,
	[Password] [varchar](255) NOT NULL,
	[ProfId] [int] NOT NULL,
	[Specialty] [nvarchar](max) NOT NULL,
	[IsOrganization] [varchar](50) NULL,
	[OrganzeName] [varchar](255) NULL,
 CONSTRAINT [PK__ServiceP__3214EC0772513B80] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ServiceSeeker]    Script Date: 7/19/2016 3:09:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ServiceSeeker](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[CityId] [int] NOT NULL,
	[AreaId] [int] NOT NULL,
	[ContactAddress] [varchar](500) NOT NULL,
	[ContactNo] [varchar](30) NOT NULL,
	[Image] [varchar](255) NULL,
	[Email] [varchar](255) NOT NULL,
	[Password] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Area] ON 

INSERT [dbo].[Area] ([AreaId], [AreaName], [CityId]) VALUES (1, N'Dhanmondi', 1)
INSERT [dbo].[Area] ([AreaId], [AreaName], [CityId]) VALUES (2, N'Farmgate', 1)
INSERT [dbo].[Area] ([AreaId], [AreaName], [CityId]) VALUES (3, N'railway colony', 3)
SET IDENTITY_INSERT [dbo].[Area] OFF
SET IDENTITY_INSERT [dbo].[City] ON 

INSERT [dbo].[City] ([CityId], [CityName]) VALUES (1, N'Dhaka')
INSERT [dbo].[City] ([CityId], [CityName]) VALUES (2, N'Chittagong')
INSERT [dbo].[City] ([CityId], [CityName]) VALUES (3, N'Tangail')
SET IDENTITY_INSERT [dbo].[City] OFF
SET IDENTITY_INSERT [dbo].[Profession] ON 

INSERT [dbo].[Profession] ([Id], [ProfName], [Description]) VALUES (1, N'Software Engineer', N'Software Engineer')
INSERT [dbo].[Profession] ([Id], [ProfName], [Description]) VALUES (2, N'Electrician', N'Electrician')
INSERT [dbo].[Profession] ([Id], [ProfName], [Description]) VALUES (3, N'Beauty Specialist', N'He is male and female beauty expert')
SET IDENTITY_INSERT [dbo].[Profession] OFF
SET IDENTITY_INSERT [dbo].[ServiceProvider] ON 

INSERT [dbo].[ServiceProvider] ([Id], [Name], [CityId], [AreaId], [ContactAddress], [ContactNo], [Image], [Email], [Password], [ProfId], [Specialty], [IsOrganization], [OrganzeName]) VALUES (1013, N'Kh Akash', 3, 3, N'23/R,Railway Coloy', N'01675432414', N'1013_1.jpg', N'kh@gmail.com', N'123456', 3, N'beauty specialist', NULL, N'Persona')
INSERT [dbo].[ServiceProvider] ([Id], [Name], [CityId], [AreaId], [ContactAddress], [ContactNo], [Image], [Email], [Password], [ProfId], [Specialty], [IsOrganization], [OrganzeName]) VALUES (1014, N'Anupam Dev', 1, 1, N'Jigatola,Gabtola Maszid Road', N'01998765432', N'1014_quote.png', N'anupam@gmail.com', N'123456', 1, N'specialist in java and jsp', NULL, N'BJ It')
SET IDENTITY_INSERT [dbo].[ServiceProvider] OFF
SET IDENTITY_INSERT [dbo].[ServiceSeeker] ON 

INSERT [dbo].[ServiceSeeker] ([Id], [Name], [CityId], [AreaId], [ContactAddress], [ContactNo], [Image], [Email], [Password]) VALUES (1, N'Abdul Gafur', 1, 1, N'Dhaka', N'01689234156', N'0_Abdul Gafur', N'khairul@balancika.com', N'123456')
INSERT [dbo].[ServiceSeeker] ([Id], [Name], [CityId], [AreaId], [ContactAddress], [ContactNo], [Image], [Email], [Password]) VALUES (4, N'Aysha Shila', 1, 1, N'24/D,West Tejturi Bazar,Dhaka-1215', N'01670176250', N'4_1654380_762850443744581_1183213547_n.jpg', N'shila@gmail.com', N'123456')
INSERT [dbo].[ServiceSeeker] ([Id], [Name], [CityId], [AreaId], [ContactAddress], [ContactNo], [Image], [Email], [Password]) VALUES (6, N'Khairul Omi', 1, 2, N'Famgate bank Colony', N'01675432414', N'6_4_1654380_762850443744581_1183213547_n.jpg', N'omi@gmail.com', N'123456')
SET IDENTITY_INSERT [dbo].[ServiceSeeker] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__ServiceP__A9D105349F74437B]    Script Date: 7/19/2016 3:09:45 PM ******/
ALTER TABLE [dbo].[ServiceProvider] ADD  CONSTRAINT [UQ__ServiceP__A9D105349F74437B] UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__ServiceS__A9D105341D96492B]    Script Date: 7/19/2016 3:09:45 PM ******/
ALTER TABLE [dbo].[ServiceSeeker] ADD UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Area]  WITH CHECK ADD FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([CityId])
GO
ALTER TABLE [dbo].[Posts]  WITH CHECK ADD  CONSTRAINT [FK_Posts_Profession] FOREIGN KEY([ProfessionId])
REFERENCES [dbo].[Profession] ([Id])
GO
ALTER TABLE [dbo].[Posts] CHECK CONSTRAINT [FK_Posts_Profession]
GO
ALTER TABLE [dbo].[Posts]  WITH CHECK ADD  CONSTRAINT [FK_Posts_ServiceSeeker] FOREIGN KEY([ServiceSeekerId])
REFERENCES [dbo].[ServiceSeeker] ([Id])
GO
ALTER TABLE [dbo].[Posts] CHECK CONSTRAINT [FK_Posts_ServiceSeeker]
GO
ALTER TABLE [dbo].[Reviews]  WITH CHECK ADD FOREIGN KEY([GivenBy])
REFERENCES [dbo].[ServiceSeeker] ([Id])
GO
ALTER TABLE [dbo].[Reviews]  WITH CHECK ADD  CONSTRAINT [FK__Reviews__Sprovid__239E4DCF] FOREIGN KEY([SproviderId])
REFERENCES [dbo].[ServiceProvider] ([Id])
GO
ALTER TABLE [dbo].[Reviews] CHECK CONSTRAINT [FK__Reviews__Sprovid__239E4DCF]
GO
ALTER TABLE [dbo].[ServiceProvider]  WITH CHECK ADD  CONSTRAINT [FK__ServicePr__AreaI__1BFD2C07] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([AreaId])
GO
ALTER TABLE [dbo].[ServiceProvider] CHECK CONSTRAINT [FK__ServicePr__AreaI__1BFD2C07]
GO
ALTER TABLE [dbo].[ServiceProvider]  WITH CHECK ADD  CONSTRAINT [FK__ServicePr__CityI__1B0907CE] FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([CityId])
GO
ALTER TABLE [dbo].[ServiceProvider] CHECK CONSTRAINT [FK__ServicePr__CityI__1B0907CE]
GO
ALTER TABLE [dbo].[ServiceProvider]  WITH CHECK ADD  CONSTRAINT [FK_ServiceProvider_Profession] FOREIGN KEY([ProfId])
REFERENCES [dbo].[Profession] ([Id])
GO
ALTER TABLE [dbo].[ServiceProvider] CHECK CONSTRAINT [FK_ServiceProvider_Profession]
GO
ALTER TABLE [dbo].[ServiceSeeker]  WITH CHECK ADD  CONSTRAINT [FK_ServiceSeeker_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([AreaId])
GO
ALTER TABLE [dbo].[ServiceSeeker] CHECK CONSTRAINT [FK_ServiceSeeker_Area]
GO
ALTER TABLE [dbo].[ServiceSeeker]  WITH CHECK ADD  CONSTRAINT [FK_ServiceSeeker_City] FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([CityId])
GO
ALTER TABLE [dbo].[ServiceSeeker] CHECK CONSTRAINT [FK_ServiceSeeker_City]
GO
USE [master]
GO
ALTER DATABASE [DailyServiceDB] SET  READ_WRITE 
GO
