//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;

namespace DailyServiceToolkit.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ServiceSeeker
    {
        public ServiceSeeker()
        {
            this.Reviews = new HashSet<Review>();
            this.Posts = new HashSet<Post>();
        }
    
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "City")]
        public int CityId { get; set; }
         [Display(Name = "Area")]
        public int AreaId { get; set; }
        [Required]
        [Display(Name = "Contact Address")]
        public string ContactAddress { get; set; }
        [Required]
        [Display(Name = "Contact No")]
        public string ContactNo { get; set; }
        [Required]
        public string Image { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    
        public virtual ICollection<Review> Reviews { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual Area Area { get; set; }
        public virtual City City { get; set; }
    }
}
