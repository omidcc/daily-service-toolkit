﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DailyServiceToolkit.Startup))]
namespace DailyServiceToolkit
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
