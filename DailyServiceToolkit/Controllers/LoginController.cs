﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DailyServiceToolkit.Models;

namespace DailyServiceToolkit.Controllers
{
     

    public class LoginController : Controller
    {
        private DailyServiceDBEntities db = new DailyServiceDBEntities();
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(string email="",string pass="")
        {
            
            string Email = Request["txtUser"];
            string Pass = Request["txtPass"];
            var sp = db.ServiceProviders.SingleOrDefault(s => s.Email == Email && s.Password == Pass);
            var sk = db.ServiceSeekers.ToList().SingleOrDefault(s => s.Email == Email   && s.Password == Pass);

            if (sp == null && sk == null)
            {
                return RedirectToAction("Index");
            }
            else if (sp != null && sk == null)
            { 

            Session["sp"] = sp;
                Session["spId"] = sp.Id;
                return RedirectToAction("Details","ServiceProvider",new{id=sp.Id});
            }
            else if (sp == null && sk!= null)
            {
                
                Session["sk"] = sk;
                Session["skId"] = sk.Id;
                return RedirectToAction("Details", "ServiceSeeker", new { id = sk.Id });
            }
            return View();
        }

        public ActionResult Logout()
        {
            Session["sp"] = null;
            Session["spId"] = null;
            Session["skId"] = null;
            Session["sk"] = null;
            
            return RedirectToAction("Index","Login");
        }
    }
}