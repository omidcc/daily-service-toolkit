﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DailyServiceToolkit.Models;

namespace DailyServiceToolkit.Controllers
{
    public class ProfileController : Controller
    {
        private DailyServiceDBEntities db = new DailyServiceDBEntities();

        // GET: /Profile/
        //public ActionResult Index()
        //{
        //    var serviceproviders = db.ServiceProviders.Include(s => s.Area).Include(s => s.City).Include(s => s.Profession);
        //    return View(serviceproviders.ToList());
        //}

        // GET: /Profile/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceProvider serviceprovider = db.ServiceProviders.Find(id);
            if (serviceprovider == null)
            {
                return HttpNotFound();
            }
            return View(serviceprovider);
        }

        // GET: /Profile/Create
        //public ActionResult Create()
        //{
        //    ViewBag.AreaId = new SelectList(db.Areas, "AreaId", "AreaName");
        //    ViewBag.CityId = new SelectList(db.Cities, "CityId", "CityName");
        //    ViewBag.ProfId = new SelectList(db.Professions, "Id", "ProfName");
        //    return View();
        //}

        //// POST: /Profile/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include="Id,Name,CityId,AreaId,ContactAddress,ContactNo,Image,Email,Password,ProfId,Specialty,IsOrganization,OrganzeName")] ServiceProvider serviceprovider)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.ServiceProviders.Add(serviceprovider);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.AreaId = new SelectList(db.Areas, "AreaId", "AreaName", serviceprovider.AreaId);
        //    ViewBag.CityId = new SelectList(db.Cities, "CityId", "CityName", serviceprovider.CityId);
        //    ViewBag.ProfId = new SelectList(db.Professions, "Id", "ProfName", serviceprovider.ProfId);
        //    return View(serviceprovider);
        //}

        //// GET: /Profile/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ServiceProvider serviceprovider = db.ServiceProviders.Find(id);
        //    if (serviceprovider == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.AreaId = new SelectList(db.Areas, "AreaId", "AreaName", serviceprovider.AreaId);
        //    ViewBag.CityId = new SelectList(db.Cities, "CityId", "CityName", serviceprovider.CityId);
        //    ViewBag.ProfId = new SelectList(db.Professions, "Id", "ProfName", serviceprovider.ProfId);
        //    return View(serviceprovider);
        //}

        //// POST: /Profile/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include="Id,Name,CityId,AreaId,ContactAddress,ContactNo,Image,Email,Password,ProfId,Specialty,IsOrganization,OrganzeName")] ServiceProvider serviceprovider)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(serviceprovider).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.AreaId = new SelectList(db.Areas, "AreaId", "AreaName", serviceprovider.AreaId);
        //    ViewBag.CityId = new SelectList(db.Cities, "CityId", "CityName", serviceprovider.CityId);
        //    ViewBag.ProfId = new SelectList(db.Professions, "Id", "ProfName", serviceprovider.ProfId);
        //    return View(serviceprovider);
        //}

        //// GET: /Profile/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ServiceProvider serviceprovider = db.ServiceProviders.Find(id);
        //    if (serviceprovider == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(serviceprovider);
        //}

        //// POST: /Profile/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ServiceProvider serviceprovider = db.ServiceProviders.Find(id);
        //    db.ServiceProviders.Remove(serviceprovider);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
