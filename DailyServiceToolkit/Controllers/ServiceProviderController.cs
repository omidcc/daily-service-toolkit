﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DailyServiceToolkit.Models;

namespace DailyServiceToolkit.Controllers
{
    public class ServiceProviderController : Controller
    {
        private DailyServiceDBEntities db = new DailyServiceDBEntities();

        // GET: /ServiceProvider/
        public ActionResult Index()
        {
            var serviceproviders = db.ServiceProviders.Include(s => s.Area).Include(s => s.City).Include(s => s.Profession);
            return View(serviceproviders.ToList());
        }

        // GET: /ServiceProvider/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceProvider serviceprovider = db.ServiceProviders.Find(id);
            if (serviceprovider == null)
            {
                return HttpNotFound();
            }
            return View(serviceprovider);
        }

        // GET: /ServiceProvider/Create
        public ActionResult Create()
        {
            ViewBag.AreaId = new SelectList(db.Areas, "AreaId", "AreaName");
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "CityName");
            ViewBag.ProfId = new SelectList(db.Professions, "Id", "ProfName");
            return View();
        }

        // POST: /ServiceProvider/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,CityId,AreaId,ContactAddress,ContactNo,Image,Email,Password,ProfId,Specialty,IsOrganization,OrganzeName")]HttpPostedFileBase image, ServiceProvider serviceprovider)
        {

            if (ModelState.IsValid)
            {
                try
                {

                    var s = serviceprovider;

                    db.ServiceProviders.Add(serviceprovider);
                    db.SaveChanges();
                    serviceprovider.Image = serviceprovider.Id + "_" + image.FileName;
                    db.SaveChanges();
                    //upload image to a folder
                    var allowedExtensions = new[] {".Jpg", ".png", ".jpg", "jpeg"};

                    var fileName = Path.GetFileName(image.FileName); //getting only file name(ex-ganesh.jpg)  
                    var ext = Path.GetExtension(image.FileName); //getting the extension(ex-.jpg)  
                    if (allowedExtensions.Contains(ext)) //check what type of extension  
                    {
                        string name = Path.GetFileNameWithoutExtension(fileName);
                        //getting file name without extension  
                        string myfile = serviceprovider.Id + "_" + image.FileName; //appending the name with id  
                        // store the file inside ~/project folder(Img)  
                        var path = Path.Combine(Server.MapPath("~/Uploads/ServiceProvider"), myfile);

                        image.SaveAs(path);
                    }
                    else
                    {
                        ViewBag.message = "Please choose only Image file";
                    }
                    ViewBag.Message = "Account created successfully! login below";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                }
                return RedirectToAction("Index","Login");
            }

            ViewBag.AreaId = new SelectList(db.Areas, "AreaId", "AreaName", serviceprovider.AreaId);
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "CityName", serviceprovider.CityId);
            ViewBag.ProfId = new SelectList(db.Professions, "Id", "ProfName", serviceprovider.ProfId);

            return View(serviceprovider);
        }
       
        public ActionResult RegisterSuccess()
        {
            
            return View();
        }


        public ActionResult SearchByProfession()
        {
            ViewBag.ProfId = new SelectList(db.Professions, "Id", "ProfName");
            ViewBag.AreaId = new SelectList(db.Areas, "AreaId", "AreaName");

            return View();
        }
        [HttpPost]
        public ActionResult SearchByProfession(int profid=0,int areaId=0)
        {
            ViewBag.ProfId = new SelectList(db.Professions, "Id", "ProfName");
            ViewBag.AreaId = new SelectList(db.Areas, "AreaId", "AreaName");
            ViewBag.PId = profid;
            ViewBag.AId = areaId;
            return View();
        }

      
        // GET: /ServiceProvider/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceProvider serviceprovider = db.ServiceProviders.Find(id);
            if (serviceprovider == null)
            {
                return HttpNotFound();
            }
            ViewBag.AreaId = new SelectList(db.Areas, "AreaId", "AreaName", serviceprovider.AreaId);
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "CityName", serviceprovider.CityId);
            ViewBag.ProfId = new SelectList(db.Professions, "Id", "ProfName", serviceprovider.ProfId);
            return View(serviceprovider);
        }

        // POST: /ServiceProvider/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,CityId,AreaId,ContactAddress,ContactNo,Image,Email,Password,ProfId,Specialty,IsOrganization,OrganzeName")] ServiceProvider serviceprovider)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviceprovider).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details",new{@id=serviceprovider.Id});
            }
            ViewBag.AreaId = new SelectList(db.Areas, "AreaId", "AreaName", serviceprovider.AreaId);
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "CityName", serviceprovider.CityId);
            ViewBag.ProfId = new SelectList(db.Professions, "Id", "ProfName", serviceprovider.ProfId);
            return View(serviceprovider);
        }

        // GET: /ServiceProvider/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceProvider serviceprovider = db.ServiceProviders.Find(id);
            if (serviceprovider == null)
            {
                return HttpNotFound();
            }
            return View(serviceprovider);
        }

        // POST: /ServiceProvider/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ServiceProvider serviceprovider = db.ServiceProviders.Find(id);
            db.ServiceProviders.Remove(serviceprovider);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
