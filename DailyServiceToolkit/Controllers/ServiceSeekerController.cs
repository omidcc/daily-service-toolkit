﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DailyServiceToolkit.Models;
using System.IO;

namespace DailyServiceToolkit.Controllers
{
    public class ServiceSeekerController : Controller
    {
        private DailyServiceDBEntities db = new DailyServiceDBEntities();

        // GET: /ServiceSeeker/
        public ActionResult Index()
        {
            return View(db.ServiceSeekers.ToList());
        }

        public JsonResult GetArea(int cityId = 0)
        {
            return Json(new SelectList(db.Areas.Where(a => a.CityId == cityId), "ID", "Name"));
        }

        // GET: /ServiceSeeker/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceSeeker serviceseeker = db.ServiceSeekers.Find(id);
            if (serviceseeker == null)
            {
                return HttpNotFound();
            }
            return View(serviceseeker);
        }

        // GET: /ServiceSeeker/Create
        public ActionResult Create()
        {

            ViewBag.AreaId = new SelectList(db.Areas, "AreaId", "AreaName");
            ViewBag.CityId = new SelectList(db.Cities,"CityId", "CityName");
            return View();

        }

        // POST: /ServiceSeeker/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,CityId,AreaId,ContactAddress,ContactNo,Image,Email,Password")] HttpPostedFileBase image, ServiceSeeker serviceseeker, int cityId = 0, int areaId = 0)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.ServiceSeekers.Add(serviceseeker);
                    db.SaveChanges();
                    serviceseeker.Image = serviceseeker.Id + "_" + image.FileName;
                    db.SaveChanges();

                    //upload image to a folder
                    var allowedExtensions = new[] {".Jpg", ".png", ".jpg", "jpeg"};

                    var fileName = Path.GetFileName(image.FileName); //getting only file name(ex-ganesh.jpg)  
                    var ext = Path.GetExtension(image.FileName); //getting the extension(ex-.jpg)  
                    if (allowedExtensions.Contains(ext)) //check what type of extension  
                    {
                        string name = Path.GetFileNameWithoutExtension(fileName);
                        //getting file name without extension  
                        string myfile = serviceseeker.Id + "_" + image.FileName; //appending the name with id  
                        // store the file inside ~/project folder(Img)  
                        var path = Path.Combine(Server.MapPath("~/Uploads/ServiceSeeker"), myfile);

                        image.SaveAs(path);
                    }
                    else
                    {
                        ViewBag.message = "Please choose only Image file";
                    }
                    ViewBag.Message = "Account created successfully! login below";
                    return RedirectToAction("Index", "Login");
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                }
            }
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "Name", cityId);
            ViewBag.AreaId = new SelectList(db.Areas.Where(ar => ar.CityId == cityId), "AreaId", "Name", areaId);
            return View(serviceseeker);
        }

        // GET: /ServiceSeeker/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceSeeker serviceseeker = db.ServiceSeekers.Find(id);
            if (serviceseeker == null)
            {
                return HttpNotFound();
            }
            return View(serviceseeker);
        }

        // POST: /ServiceSeeker/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,CityId,AreaId,ContactAddress,ContactNo,Image,Email,Password")] ServiceSeeker serviceseeker)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviceseeker).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details","ServiceSeeker",new{@id=serviceseeker.Id});
            }
            ViewBag.AreaId = new SelectList(db.Areas, "AreaId", "AreaName", serviceseeker.AreaId);
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "CityName", serviceseeker.CityId);
            return View(serviceseeker);
        }

        // GET: /ServiceSeeker/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceSeeker serviceseeker = db.ServiceSeekers.Find(id);
            if (serviceseeker == null)
            {
                return HttpNotFound();
            }
            return View(serviceseeker);
        }

        // POST: /ServiceSeeker/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ServiceSeeker serviceseeker = db.ServiceSeekers.Find(id);
            db.ServiceSeekers.Remove(serviceseeker);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
