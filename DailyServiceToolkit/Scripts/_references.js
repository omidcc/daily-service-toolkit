﻿/// <autosync enabled="true" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="bootstrap.js" />
/// <reference path="respond.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="../assets/js/pages/calendar.js" />
/// <reference path="../assets/js/pages/charts-chartjs.js" />
/// <reference path="../assets/js/pages/charts-flot.js" />
/// <reference path="../assets/js/pages/charts-morris.js" />
/// <reference path="../assets/js/pages/charts-sparkline.js" />
/// <reference path="../assets/js/pages/coming-soon.js" />
/// <reference path="../assets/js/pages/compose.js" />
/// <reference path="../assets/js/pages/contact.js" />
/// <reference path="../assets/js/pages/dashboard.js" />
/// <reference path="../assets/js/pages/form-elements.js" />
/// <reference path="../assets/js/pages/form-image-crop.js" />
/// <reference path="../assets/js/pages/form-select2.js" />
/// <reference path="../assets/js/pages/form-wizard.js" />
/// <reference path="../assets/js/pages/form-x-editable.js" />
/// <reference path="../assets/js/pages/gallery.js" />
/// <reference path="../assets/js/pages/inbox.js" />
/// <reference path="../assets/js/pages/jstree.js" />
/// <reference path="../assets/js/pages/maps-google.js" />
/// <reference path="../assets/js/pages/maps-vector.js" />
/// <reference path="../assets/js/pages/nestable.js" />
/// <reference path="../assets/js/pages/notifications.js" />
/// <reference path="../assets/js/pages/profile.js" />
/// <reference path="../assets/js/pages/shop.js" />
/// <reference path="../assets/js/pages/table-data.js" />
/// <reference path="../assets/js/pages/timeline.js" />
/// <reference path="../assets/plugins/curvedlines/curvedlines.js" />
/// <reference path="../assets/plugins/jquery-blockui/jquery.blockui.js" />
/// <reference path="../assets/plugins/jquery-mockjax-master/jquery.mockjax.js" />
/// <reference path="../assets/plugins/moment/moment.js" />
/// <reference path="../assets/plugins/3d-bold-navigation/js/main.js" />
/// <reference path="../assets/plugins/3d-bold-navigation/js/modernizr.js" />
/// <reference path="../assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" />
/// <reference path="../assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" />
/// <reference path="../assets/plugins/gridgallery/js/cbpgridgallery.js" />
/// <reference path="../assets/plugins/gridgallery/js/classie.js" />
/// <reference path="../assets/plugins/jqvmap/data/jquery.vmap.sampledata.js" />
/// <reference path="../assets/plugins/jqvmap/maps/jquery.vmap.world.js" />
/// <reference path="../assets/plugins/offcanvasmenueffects/js/classie.js" />
/// <reference path="../assets/plugins/offcanvasmenueffects/js/main.js" />
/// <reference path="../assets/plugins/offcanvasmenueffects/js/snap.svg-min.js" />
/// <reference path="../assets/plugins/pricing-tables/js/main.js" />
/// <reference path="../assets/plugins/product-preview-slider/js/main.js" />
/// <reference path="../assets/plugins/vertical-timeline/js/main.js" />
/// <reference path="../assets/plugins/x-editable/bootstrap3-editable/js/bootstrap-editable.js" />
/// <reference path="../assets/plugins/x-editable/inputs-ext/address/address.js" />
/// <reference path="../assets/plugins/x-editable/inputs-ext/typeaheadjs/typeaheadjs.js" />
/// <reference path="../assets/plugins/x-editable/inputs-ext/typeaheadjs/lib/typeahead.js" />
/// <reference path="../assets/js/modern.js" />
/// <reference path="../assets/plugins/chartsjs/chart.js" />
/// <reference path="../assets/plugins/dropzone/dropzone.js" />
/// <reference path="../assets/plugins/flot/jquery.flot.js" />
/// <reference path="../assets/plugins/flot/jquery.flot.pie.js" />
/// <reference path="../assets/plugins/flot/jquery.flot.resize.js" />
/// <reference path="../assets/plugins/flot/jquery.flot.symbol.js" />
/// <reference path="../assets/plugins/flot/jquery.flot.time.js" />
/// <reference path="../assets/plugins/flot/jquery.flot.tooltip.js" />
/// <reference path="../assets/plugins/fullcalendar/fullcalendar.js" />
/// <reference path="../assets/plugins/image-cropper/cropper.js" />
/// <reference path="../assets/plugins/jquery/jquery-2.1.3.js" />
/// <reference path="../assets/plugins/jquery-countdown/jquery.countdown.js" />
/// <reference path="../assets/plugins/jquery-counterup/jquery.counterup.js" />
/// <reference path="../assets/plugins/jquery-slimscroll/jquery.slimscroll.js" />
/// <reference path="../assets/plugins/jquery-sparkline/jquery.sparkline.js" />
/// <reference path="../assets/plugins/jquery-ui/jquery-ui.js" />
/// <reference path="../assets/plugins/jquery-validation/jquery.validate.js" />
/// <reference path="../assets/plugins/jqvmap/jquery.vmap.js" />
/// <reference path="../assets/plugins/jstree/jstree.js" />
/// <reference path="../assets/plugins/metrojs/metrojs.js" />
/// <reference path="../assets/plugins/morris/morris.js" />
/// <reference path="../assets/plugins/morris/raphael.js" />
/// <reference path="../assets/plugins/pace-master/pace.js" />
/// <reference path="../assets/plugins/summernote-master/summernote.js" />
/// <reference path="../assets/plugins/switchery/switchery.js" />
/// <reference path="../assets/plugins/toastr/toastr.js" />
/// <reference path="../assets/plugins/twitter-bootstrap-wizard/jquery.bootstrap.wizard.js" />
/// <reference path="../assets/plugins/uniform/jquery.uniform.js" />
/// <reference path="../assets/plugins/waves/waves.js" />
/// <reference path="../assets/plugins/waypoints/jquery.waypoints.js" />
/// <reference path="../assets/plugins/bootstrap/js/bootstrap.js" />
/// <reference path="../assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" />
/// <reference path="../assets/plugins/datatables/js/jquery.datatables.js" />
/// <reference path="../assets/plugins/fullcalendar/lib/moment.js" />
/// <reference path="../assets/plugins/gridgallery/js/imagesloaded.pkgd.js" />
/// <reference path="../assets/plugins/gridgallery/js/masonry.pkgd.js" />
/// <reference path="../assets/plugins/product-preview-slider/js/jquery.mobile.js" />
/// <reference path="../assets/plugins/select2/js/select2.full.js" />
/// <reference path="../assets/plugins/select2/js/select2.js" />
/// <reference path="../content/kendo.all.js" />
